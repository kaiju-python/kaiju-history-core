FROM alpine:3.13 as python-base

ENV PORT 8700
ENV PYTHONUNBUFFERED 1
ENV PYTHONOPTIMIZE 2
WORKDIR /app
ENTRYPOINT ["./entrypoint.sh"]
EXPOSE ${PORT}
STOPSIGNAL SIGINT

RUN apk --update --no-cache add python3 && \
    if [ ! -e /usr/bin/python ]; then ln -sf python3 /usr/bin/python ; fi && \
    python3 -m ensurepip && \
    rm -r /usr/lib/python*/ensurepip && \
    rm -r /usr/lib/python*/lib2to3 && \
    rm -r /usr/lib/python*/turtledemo && \
    rm /usr/lib/python*/turtle.py && \
    rm /usr/lib/python*/webbrowser.py && \
    pip3 install --no-cache --upgrade pip setuptools wheel && \
    if [ ! -e /usr/bin/pip ]; then ln -s pip3 /usr/bin/pip ; fi && \
    rm -rf /root/.cache /var/cache /usr/share/terminfoapk


FROM python-base AS dependencies

COPY requirements.alpine requirements.alpine.build ./
RUN apk update && \
    apk --no-cache --virtual app-deps add $(cat requirements.alpine | grep -v '#' | xargs) && \
    apk --no-cache --virtual app-deps-build add $(cat requirements.alpine.build | grep -v '#' | xargs) && \
    rm -rf /root/.cache /var/cache /usr/share/terminfoapk

RUN git clone https://github.com/acsylla/acsylla.git
RUN make --directory ./acsylla install-driver

FROM dependencies as requirements

COPY requirements.txt ./
RUN pip3 install --no-cache-dir -r requirements.txt && \
    apk del app-deps-build

FROM requirements as code

COPY entrypoint.sh ./
COPY fixtures ./fixtures
COPY app/ ./app
COPY settings/ ./settings
COPY docs/ ./docs
RUN find / -type d -name __pycache__ -exec rm -r {} +
