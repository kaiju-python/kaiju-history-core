import acsylla

from kaiju_tools.services import ContextableService


class ScyllaService(ContextableService):
    service_name = 'scylla'
    keyspace: str = None
    cluster: acsylla.Cluster = None

    def __init__(self, app, keyspace, logger=None, **kwargs):
        """
        :param app:
        :param keyspace:
        :param kwargs: {
            contact_points: List[str],
            port: int = 9042,
            username: str = None,
            password: str = None,
            protocol_version: int = 3,
            connect_timeout: float = 5.0,
            request_timeout: float = 2.0,
            resolve_timeout: float = 1.0,
            consistency: Consistency = Consistency.LOCAL_ONE,
            core_connections_per_host: int = 1,
            local_port_range_min: int = 49152,
            local_port_range_max: int = 65535,
            application_name: str = "acsylla",
            application_version: str = __version__,
            num_threads_io: int = 1,
            ssl_enabled: bool = False,
            ssl_cert: str = None,
            ssl_private_key: str = None,
            ssl_private_key_password: str = "",
            ssl_trusted_cert: str = None,
            ssl_verify_flags: SSLVerifyFlags = SSLVerifyFlags.PEER_CERT,
        }
        """
        self.conf = kwargs
        super().__init__(app=app, logger=logger)
        self.keyspace = keyspace

    async def init(self):
        self.cluster = acsylla.create_cluster(**self.conf)
        self.cluster

    @property
    def db(self):
        return ScyllaSession(self.app, keyspace=self.keyspace)


class ScyllaSession:
    session: acsylla.Session = None

    def __init__(self, app, keyspace=None):
        self.app = app
        self.keyspace = keyspace

    async def __aenter__(self):
        self.session = await self.app.services.scylla.cluster.create_session(
            keyspace=self.keyspace or self.app.services.scylla.keyspace)
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        await self.session.close()
