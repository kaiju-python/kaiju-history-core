"""
Import all your custom services here.
"""

from .cassandra.service import ScyllaService
from .history import HistoryService
