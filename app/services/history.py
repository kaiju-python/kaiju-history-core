from pathlib import Path
from typing import Union, Optional

import acsylla

from kaiju_tools.fixtures import BaseFixtureService
from kaiju_tools.rpc.abc import AbstractRPCCompatible


class HistoryService(BaseFixtureService, AbstractRPCCompatible):
    service_name = "History"

    @property
    def routes(self):
        return {
            'create': self.create,

        }

    @property
    def permissions(self):
        return {
            self.DEFAULT_PERMISSION: self.PermissionKeys.GLOBAL_GUEST_PERMISSION
        }

    def load_file(self, filename: Union[Path, str]) -> Optional:
        """
        Loads a fixture from a file. Returns None if file doesn't exist.
        """

        if isinstance(filename, str):
            filename = self._base_dir / filename

        if not filename.exists() or filename.is_dir():
            return None
        else:
            with open(str(filename), 'r', encoding="utf8") as f:
                return f.read()

    async def init(self):
        """create table"""
        file = self.load_file("db.cql")
        async with self.app.services.scylla.db as db:
            """CREATE DATA BASE"""
            statement = acsylla.create_statement(file)
            await db.session.execute(statement)

    async def create(self):
        CQL = "INSERT 7777"

        statement = acsylla.create_statement(CQL)

        async with self.app.services.scylla.db as db:
            result = await db.session.execute(statement)
