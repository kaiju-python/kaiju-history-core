"""

Running the app
---------------

You have multiple ways to configure the app on its start.

ConfigLoader
^^^^^^^^^^^^

The config loader initialization consists of several stages.

1. Load base config file (template)
2. Use a local config if --cfg flag is provided, otherwise use a `default_config_path`.
   Update the base config with the local config recursively.
3. Load an env file if --env-file flag is provided, otherwise use a `default_env_path`.
   Fill the config template with env variables.
4. Load env vars from OS environment. Fill the config template with env variables.
5. Load env vars from CLI if --env <key>=<value> flags were provided.
   Fill the config template with env variables.
6. Install dependencies specified in the "requirements" section of the config
   unless --no-deps flag was specified in CLI.
7. Import services from the dependencies if any of them available in each
   dependency `.services` module.

After all, a new `Config` object is returned.

Examples
^^^^^^^^

Load a default config but override some env variables.

.. code-block:: console

    python -m app --env port=9999

Load a different config file overriding the base config parameters with it.

    python -m app --cfg=local.yml

Use an environment variable.

    export port=5555
    python -m app


"""

from kaiju_tools.init_app import main

# IMPORT FOR BUILD
# from asyncpg.pgproto.pgproto import *
# import sqlalchemy.sql.default_comparator
# import sqlalchemy.util.langhelpers


from app.application import init_app
from app.commands import *

# -- run! ---------------------------------------------------------------------

if __name__ == '__main__':
    main(init_app)
